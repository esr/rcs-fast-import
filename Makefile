#
# makefile for rcs-fast-import
#
VERS=$(shell sed <rcs-fast-import -n -e '/version *= \"*\(.*\)\"/s//\1/p')

SOURCES = README COPYING NEWS rcs-fast-import rcs-fast-import.adoc Makefile control

all: rcs-fast-import.1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

# The regression test
check:
	@cd test >/dev/null; $(MAKE) --quiet

spellcheck:
	batchspell local.dic rcs-fast-import.adoc rcs-fast-import README

clean:
	rm -f  *~ *.1 *.html *.tar.gz MANIFEST
	rm -fr .rs* typescript test/typescript

rcs-fast-import-$(VERS).tar.gz: $(SOURCES) rcs-fast-import.1 
	@ls $(SOURCES) rcs-fast-import.1 | sed s:^:rcs-fast-import-$(VERS)/: >MANIFEST
	@(cd ..; ln -s rcs-fast-import rcs-fast-import-$(VERS))
	(cd ..; tar -czvf rcs-fast-import/rcs-fast-import-$(VERS).tar.gz `cat rcs-fast-import/MANIFEST`)
	@(cd ..; rm rcs-fast-import-$(VERS))

pylint:
	@pylint --score=n rcs-fast-import

dist: rcs-fast-import-$(VERS).tar.gz

NEWSVERSION=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

version:
	@echo "From program:" $(VERS) "From NEWSL" $(NEWSVERSION)

release: rcs-fast-import-$(VERS).tar.gz rcs-fast-import.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper version=$(VERS) | sh -e -x

refresh: rcs-fast-import.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper -N -w version=$(VERS) | sh -e -x

